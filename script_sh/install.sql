CREATE DATABASE data_esd
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_general_ci;
    
USE data_esd;

CREATE TABLE user(
    UserID int AUTO_INCREMENT PRIMARY KEY,
    LastName varchar(255),
    FirstName varchar(255),
    Password varchar(255),
    Email varchar(255)
);